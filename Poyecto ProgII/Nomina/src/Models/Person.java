/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

/**
 *
 * @author Elias Vega
 */
        
public class Person {
    
    private String FirstName, SecondName, LastName, SecondLastName ;
    private int Salary, WorkDays;
    private double devengos, deducciones;

    public Person(String FirstName, String SecondName, String LastName, String SecondLastName, int Salary, int WorkDays, double devengos, double deducciones) {
        this.FirstName = FirstName;
        this.SecondName = SecondName;
        this.LastName = LastName;
        this.SecondLastName = SecondLastName;
        this.Salary = Salary;
        this.WorkDays = WorkDays;
        this.devengos = devengos;
        this.deducciones = deducciones;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String SecondName) {
        this.SecondName = SecondName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getSecondLastName() {
        return SecondLastName;
    }

    public void setSecondLastName(String SecondLastName) {
        this.SecondLastName = SecondLastName;
    }

    public int getSalary() {
        return Salary;
    }

    public void setSalary(int Salary) {
        this.Salary = Salary;
    }

    public int getWorkDays() {
        return WorkDays;
    }

    public void setWorkDays(int WorkDays) {
        this.WorkDays = WorkDays;
    }

    public double getDevengos() {
        return devengos;
    }

    public void setDevengos(double devengos) {
        this.devengos = devengos;
    }

    public double getDeducciones() {
        return deducciones;
    }

    public void setDeducciones(double deducciones) {
        this.deducciones = deducciones;
    }
    
    
}
