/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.Datos;
import Views.AdministracionForm;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.JTextField;

/**
 *
 * @author Bismark Steve Garcia
 */
public class TableControllers implements ActionListener{ 
private AdministracionForm Administracion;
private JFileChooser dialog;


public TableControllers(AdministracionForm pf){
    Administracion = pf;
    dialog = new JFileChooser();
}



    
    @Override
     public void actionPerformed(ActionEvent e) {
         
         switch(e.getActionCommand()){
             case "save":
                save();
                break;
             case "clear":
                 clear();
                break;
             case "select":
                 select();
                break;       
         }

    }
      private void save(){
        dialog.showSaveDialog(Administracion);
        if(dialog.getSelectedFile()!=null){
          writeFile(dialog.getSelectedFile(), Administracion.getDatosGenerales());
        }

    }
    private void clear(){
        Administracion.clear();
    }
    
    private void select(){
        dialog.showOpenDialog(Administracion);
        if(dialog.getSelectedFile()!=null){
            AdministracionForm p = readFile(dialog.getSelectedFile());
            Administracion.showData(p);
        }
    }
    
    private void writeFile(File file, Datos Dato ){
        try {
            ObjectOutputStream w = new ObjectOutputStream(new FileOutputStream(file));
            w.writeObject(Dato);
            w.flush();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TableControllers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableControllers.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private AdministracionForm readFile(File file){
        Datos Dato = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
            Dato = (Datos)in.readObject();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TableControllers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TableControllers.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(TableControllers.class.getName()).log(Level.SEVERE, null, ex);
        }
    AdministracionForm Datos = null;
        return Datos;
    }

  
}
