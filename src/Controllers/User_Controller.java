/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controllers;

import Models.KeyChain;
import Views.Admin;
import Views.Register;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import main.Main;

/**
 *
 * @author usuario
 */
public class User_Controller implements ActionListener {
    private Admin admin;
    private Register register;
    private KeyChain keyChain;

    public User_Controller (Admin admin){
        this.admin = admin;
        keyChain = new KeyChain();
        keyChain.init();
    }
    
    public User_Controller (Register register){
        this.register = register;
        keyChain = new KeyChain();
        keyChain.init();
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        switch(e.getActionCommand()){
            case "admin.enter":
                admin();
                break;
            case "admin.exit":
                this.admin.dispose();
                break;
            case "register.register":
                register();
                break;
            case "register.cancel":
                this.register.dispose();
                break;
<<<<<<< HEAD

            case "admin.register":
                this.admin.dispose();
                Main.showRegisterForm();
                break;

=======
>>>>>>> 1f4d592db09a44260ea6cf7c824ed0f2c05bed6b
                
        }
    }

    public void register() {
        keyChain.add(this.register.getData());
        if(keyChain.save()){
            JOptionPane.showMessageDialog(register, "Tu cuenta ha sido registrada correctamente. Ahora usa tus datos para iniciar sesión.", "Registrar", JOptionPane.INFORMATION_MESSAGE);
            this.register.dispose();
            Main.showAdminForm();
        }
        else{
            JOptionPane.showMessageDialog(register, "El usuario no pudo ser registrado.", "Registrar", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    public void admin() {
        if(keyChain.isAuthorized(this.admin.getData())){
            this.admin.dispose();
            Main.showMainForm();
        }
        else
        {
            JOptionPane.showMessageDialog(admin, "El intento de inicio de sesión ha fallado.", "Admin", JOptionPane.ERROR_MESSAGE);
        }
    }
}
