/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Models;

import java.io.Serializable;

/**
 *
 * @author Bismark Steve Garcia
 */
public class Datos implements Serializable {
    private int sueldo;
    private String NameTrabajador;
    private  double deducciones;
    private double subsidio;
    private double totaldeducciones;

    public int getSueldo() {
        return sueldo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    public String getNameTrabajador() {
        return NameTrabajador;
    }

    public void setNameTrabajador(String NameTrabajador) {
        this.NameTrabajador = NameTrabajador;
    }

    public double getDeducciones() {
        return deducciones;
    }

    public void setDeducciones(double deducciones) {
        this.deducciones = deducciones;
    }

    public double getSubsidio() {
        return subsidio;
    }

    public void setSubsidio(double subsidio) {
        this.subsidio = subsidio;
    }

    public double getTotaldeducciones() {
        return totaldeducciones;
    }

    public void setTotaldeducciones(double totaldeducciones) {
        this.totaldeducciones = totaldeducciones;
    }
    
    public void getDatosGenerales(String nameTrabajador, double Deducciones, double Subsidio,int Sueldo, double Totaldeducciones){
        this.NameTrabajador = nameTrabajador;
        this.deducciones = Deducciones;
        this.subsidio = Subsidio;
        this.sueldo = Sueldo;
        this.totaldeducciones = Totaldeducciones;
    }
}
